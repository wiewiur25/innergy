﻿using System.Collections.Generic;

namespace Warehouses.Interfaces
{
    public interface ICommentFilter
    {
        IEnumerable<string> FilterCommented(IEnumerable<string> lines);
    }
}