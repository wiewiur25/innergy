﻿using System.Collections.Generic;

namespace Warehouses.Interfaces
{
    public interface IConsoleReader
    {
        List<string> ReadLines();
    }
}