﻿using Warehouses.Model;

namespace Warehouses.Interfaces
{
    public interface IMaterialFactory
    {
        Material Create(string materialId, string countOfMaterial);
        Material Create(string materialId, int countOfMaterial);
    }
}