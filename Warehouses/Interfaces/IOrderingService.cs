﻿using System.Collections.Generic;
using Warehouses.Model;

namespace Warehouses.Interfaces
{
    public interface IOrderingService<T>
    {
        IEnumerable<T> Order(IEnumerable<T> listToOrder);
    }
}