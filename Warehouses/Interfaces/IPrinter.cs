﻿using System.Collections.Generic;

namespace Warehouses.Interfaces
{
    public interface IPrinter<T>
    {
        void Print(IEnumerable<T> results);
    }
}