﻿using System.Collections.Generic;
using Warehouses.Model;

namespace Warehouses.Interfaces
{
    public interface IWarehouseContainer
    {
        Warehouse Get(string warehouseName);
        IEnumerable<Warehouse> GetAll();
    }
}