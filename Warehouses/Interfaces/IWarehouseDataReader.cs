﻿using System.Collections.Generic;
using Warehouses.Model;

namespace Warehouses.Interfaces
{
    public interface IWarehouseDataReader
    {
        IEnumerable<Warehouse> Read(IEnumerable <string> lines);
    }
}