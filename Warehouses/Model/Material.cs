﻿namespace Warehouses.Model
{
    public class Material
    {
        public string Id { get; set; }
        public int Count { get; set; }

        public Material( string id, int count)
        {
            Id = id;
            Count = count;
        }
    }
}