﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Warehouses.Model
{
    public class Warehouse
    {
        private readonly List<Material> _materials;
        public string Name { get; }
        public string NameWithTotalCount => $"{Name} (total {MaterialTotalCount})";
        public int MaterialTotalCount => _materials.Sum(m => m.Count);
        public IEnumerable<Material> Materials => _materials;

        public Warehouse(string name)
        {
            Name = name;
            _materials = new List<Material>();
        }
        public void AddMaterial(Material material) => _materials.Add(material);
    }
}