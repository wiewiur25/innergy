﻿using System;
using System.Linq.Expressions;
using SimpleInjector;
using Warehouses.Interfaces;
using Warehouses.Model;
using Warehouses.Services;

namespace Warehouses
{
    class Program
    {
        

        static void Main(string[] args)
        {
            var container = new Container();
            Initialize(container);
            
            var dataReader = container.GetInstance<IWarehouseDataReader>();
            var consoleReader = container.GetInstance<IConsoleReader>();
            var warehousePrinter = container.GetInstance<IPrinter<Warehouse>>();

            Console.WriteLine("Paste data to parse and press enter:");
            var lines = consoleReader.ReadLines();
            var results = dataReader.Read(lines);
            warehousePrinter.Print(results);
            Console.ReadKey();
        }

        private static void Initialize(Container container)
        {
            container.Register<IWarehouseDataReader, WarehouseDataReader>();
            container.Register<IWarehouseContainer, WarehouseContainer>();
            container.Register<IMaterialFactory, MaterialFactory>();
            container.Register<ICommentFilter, CommentFilter>();
            container.Register<IPrinter<Warehouse>, WarehousePrinter>();
            container.Register<IPrinter<Material>, MaterialPrinter>();
            container.Register<IOrderingService<Warehouse>, WarehouseOrderingService>();
            container.Register<IOrderingService<Material>, MaterialOrderingService>();
            container.Register<IConsoleReader, ConsoleReader>();
        }
    }
}
