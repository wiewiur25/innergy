﻿using System.Collections.Generic;
using System.Linq;
using Warehouses.Interfaces;

namespace Warehouses.Services
{
    public class CommentFilter : ICommentFilter
    {
        private readonly char _commentSign = '#';
        
        public IEnumerable<string> FilterCommented(IEnumerable<string> lines)
        {
            return lines.Where(l => !l.StartsWith(_commentSign));
        }
    }
}