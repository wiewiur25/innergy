﻿using System;
using System.Collections.Generic;
using Warehouses.Interfaces;

namespace Warehouses.Services
{
    public class ConsoleReader : IConsoleReader
    {
        public List<string> ReadLines()
        {
            var lines = new List<string>();
            string line;
            while ((line = Console.ReadLine()) != null && line != "")
            {
                lines.Add(line);
            }

            return lines;
        }
    }
}