﻿using Warehouses.Interfaces;
using Warehouses.Model;

namespace Warehouses.Services
{
    public class MaterialFactory : IMaterialFactory
    {
        public Material Create(string materialId, string countOfMaterial)
        {
            return new Material(materialId, int.Parse(countOfMaterial));
        }

        public Material Create(string materialId, int countOfMaterial)
        {
            return new Material(materialId, countOfMaterial);
        }
    }
}