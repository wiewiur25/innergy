﻿using System.Collections.Generic;
using System.Linq;
using Warehouses.Interfaces;
using Warehouses.Model;

namespace Warehouses.Services
{
    public class MaterialOrderingService : IOrderingService<Material>
    {
        public IEnumerable<Material> Order(IEnumerable<Material> listToOrder)
        {
            return listToOrder.OrderBy(m => m.Id);
        }
    }
}