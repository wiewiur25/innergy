﻿using System;
using System.Collections.Generic;
using System.Linq;
using Warehouses.Interfaces;
using Warehouses.Model;

namespace Warehouses.Services
{
    public class MaterialPrinter : IPrinter<Material>
    {
        private readonly IMaterialFactory _materialFactory;
        private readonly IOrderingService<Material> _orderingService;

        public MaterialPrinter(IMaterialFactory materialFactory, IOrderingService<Material> orderingService)
        {
            _materialFactory = materialFactory;
            _orderingService = orderingService;
        }
        public void Print(IEnumerable<Material> results)
        {
            var groupedMaterial = results
                .GroupBy(m => m.Id)
                .Select(gm => _materialFactory.Create(gm.Key, gm.Sum(m =>m.Count)));
            foreach (var material in _orderingService.Order(groupedMaterial))
            {
                Console.WriteLine($"{material.Id}: {material.Count}");
            }
        }
    }
}