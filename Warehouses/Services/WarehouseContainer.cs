﻿using System.Collections.Generic;
using System.Linq;
using Warehouses.Interfaces;
using Warehouses.Model;

namespace Warehouses.Services
{
    public class WarehouseContainer : IWarehouseContainer
    {
        private readonly List<Warehouse> _warehouses;

        public WarehouseContainer()
        {
            _warehouses = new List<Warehouse>();
        }
        public Warehouse Get(string warehouseName)
        {
            var warehouse = _warehouses.SingleOrDefault(w => w.Name == warehouseName);
            if (warehouse == null)
            {
                warehouse = new Warehouse(warehouseName);
                _warehouses.Add(warehouse);
            }

            return warehouse;
        }

        public IEnumerable<Warehouse> GetAll()
        {
            return _warehouses;
        }
    }
}