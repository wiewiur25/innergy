﻿using System;
using System.Collections.Generic;
using System.Linq;
using Warehouses.Interfaces;
using Warehouses.Model;

namespace Warehouses.Services
{
    public class WarehouseDataReader : IWarehouseDataReader
    {
        private readonly IWarehouseContainer _warehouseContainer;
        private readonly IMaterialFactory _materialFactory;
        private readonly char _materialDataSeparator = ';';
        private readonly char _warehouseDataSeparator = ',';
        private readonly char _warehouseSeparator = '|';
        private readonly ICommentFilter _commentFilter;

        public WarehouseDataReader(IWarehouseContainer warehouseContainer, IMaterialFactory materialFactory, ICommentFilter commentFilter)
        {
            _warehouseContainer = warehouseContainer;
            _materialFactory = materialFactory;
            _commentFilter = commentFilter;
        }
        public IEnumerable<Warehouse> Read(IEnumerable<string> lines)
        {
            foreach (var line in _commentFilter.FilterCommented(lines))
            {
                ReadLine(line);
            }
            return _warehouseContainer.GetAll();
        }

        private void ReadLine(string line)
        {
            var materialDataString = line.Split(_materialDataSeparator);
            var warehousesStrings = materialDataString.Last().Split(_warehouseSeparator);
            foreach (var warehouseData in warehousesStrings)
            {
                var warehouseDataStrings = warehouseData.Split(_warehouseDataSeparator);
                var warehouse = _warehouseContainer.Get(warehouseDataStrings[0]);
                var material = _materialFactory.Create(materialDataString[1], warehouseDataStrings[1]);
                warehouse.AddMaterial(material);
            }
        }
    }
}