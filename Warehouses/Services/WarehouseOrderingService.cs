﻿using System;
using System.Collections.Generic;
using System.Linq;
using Warehouses.Interfaces;
using Warehouses.Model;

namespace Warehouses.Services
{
    public class WarehouseOrderingService : IOrderingService<Warehouse>
    {
        public IEnumerable<Warehouse> Order(IEnumerable<Warehouse> listToOrder)
        {
            return listToOrder.OrderByDescending(wh => wh.MaterialTotalCount).ThenByDescending(wh => wh.Name);
        }
    }
}