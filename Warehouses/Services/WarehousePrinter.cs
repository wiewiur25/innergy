﻿using System;
using System.Collections;
using System.Collections.Generic;
using Warehouses.Interfaces;
using Warehouses.Model;

namespace Warehouses.Services
{
    public class WarehousePrinter : IPrinter<Warehouse>
    {
        private readonly IOrderingService<Warehouse> _orderingService;
        private readonly IPrinter<Material> _materialPrinter;

        public WarehousePrinter(IOrderingService<Warehouse> orderingService, IPrinter<Material> materialPrinter)
        {
            _orderingService = orderingService;
            _materialPrinter = materialPrinter;
        }
        public void Print(IEnumerable<Warehouse> results)
        {
            foreach (var warehouse in _orderingService.Order(results))
            {
                Console.WriteLine(warehouse.NameWithTotalCount); 
                _materialPrinter.Print(warehouse.Materials);
                Console.WriteLine();
            }
        }
    }
}